# Delta Hand Pie Internal


## Installation
- Requires PHP 7.2+
- Requires Composer
- How to start:
 1. run 'composer install' in cmd
 2. run 'npm install' in cmd
 3. run 'npm run dev' in cmd
 4. copy .env.example into .env
 5. Setup database info in .env
 6. Migrate database
 7. run 'php artisan key:generate' in cmd
